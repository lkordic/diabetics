package hr.etfos.diabetics;

import android.app.FragmentManager;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.os.Process;
import android.widget.Toast;

import org.w3c.dom.Text;

public class HomeScreenActivity extends AppCompatActivity {
    DBHelper myHelper;
    Cursor provjeri;
    Cursor opet_provjeri;
    TextView glukoza_zadnje;
    TextView inzulin_zadnje;
    TextView ime_prezime;
    TextView glukoza, inzulin, krvni_tlak, masa, ugljikohidrati, masti, kalorije;



    public double prosjek(double... numbers){

        double zbroj = 0;
        int counter=0;
        for (double number : numbers){
            zbroj +=number;
            counter++;
        }
        return (zbroj/counter);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        myHelper = new DBHelper(this);


        Button novo = (Button) findViewById(R.id.btn_novo);
        Button alarm = (Button) findViewById(R.id.btn_podsjetnik);
        glukoza_zadnje = (TextView)findViewById(R.id.glucose_last);
        inzulin_zadnje = (TextView)findViewById(R.id.insulin_last);
        ime_prezime = (TextView) findViewById(R.id.tv_ime_prezime);
        glukoza = (TextView) findViewById(R.id.glukoza_vrijednost);
        inzulin = (TextView) findViewById(R.id.inzulin_vrijednost);
        krvni_tlak = (TextView) findViewById(R.id.bp_vrijednost);
        masa= (TextView) findViewById(R.id.masa_vrijednost);
        masti = (TextView) findViewById(R.id.masti_vrijednost);
        ugljikohidrati = (TextView) findViewById(R.id.ugljikohidrati_vrijednost);
        kalorije = (TextView) findViewById(R.id.kalorije_vrijednost);

        provjeri = myHelper.getAllDataForUser();
        if ((provjeri.getCount()) == 0){
            startActivity(new Intent(this, MainActivity.class));
        }

        if ((provjeri.getCount()) > 0) ime_prezime.setText(myHelper.getUserName() + "" + myHelper.getUserLastname());

        //pri pokretanju aktivnosti
        opet_provjeri = myHelper.getAllData();
        if ((opet_provjeri.getCount()) > 0) {
            glukoza_zadnje.setText(myHelper.getLastGlucoseEntry().toString() + "mmol/L");
            inzulin_zadnje.setText(myHelper.getLastInsulinEntry().toString() + "U/h");
            double average = prosjek(myHelper.getAvgGlucose());
            glukoza.setText(String.valueOf(average));
            double avginz = prosjek(myHelper.getAvgInsulin());
            inzulin.setText(String.valueOf(avginz));
            double avg_bp = prosjek(myHelper.getAvgBp());
            krvni_tlak.setText(String.valueOf(avg_bp));
            double avg_carbs = prosjek(myHelper.getAvgCarbs());
            ugljikohidrati.setText(String.valueOf(avg_carbs));
            double avg_cal = prosjek(myHelper.getAvgCalories());
            kalorije.setText(String.valueOf(avg_cal));
            double avg_mass = prosjek(myHelper.getAvgMass());
            masa.setText(String.valueOf(avg_mass));
            double avg_fat = prosjek(myHelper.getAvgFat());
            masti.setText(String.valueOf(avg_fat));
        }


        assert alarm != null;
        alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //pokretanje alarm aktivnosti
                startActivity(new Intent(HomeScreenActivity.this, Alarm_activity.class));
            }
        });


        assert novo != null;
        novo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeScreenActivity.this, Values_activity.class));
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on THE Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.quit)
            onDestroy();

        //noinspection SimplifiableIfStatement
        if (id == R.id.promjeni) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("test", "edit");
            intent.putExtra("ime", myHelper.getUserName());
            intent.putExtra("prezime", myHelper.getUserLastname());
            intent.putExtra("visina", myHelper.getUserHeight());
            intent.putExtra("radio1", myHelper.getRadioButton1Value());
            intent.putExtra("radio2", myHelper.getRadioButton2Value());
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        System.exit(0);
    }
}


package hr.etfos.diabetics;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.SQLException;


//----------------------INNER CLASS - HELPER
public class DBHelper extends SQLiteOpenHelper {
    private Context context;


    private static final int DATABASE_VERSION = 7;
    private static final String DATABASE_NAME = "Dijabeticari.db";
    public static final String TABLE_1_NAME = "korisnik";
    public static final String TABLE_2_NAME = "podaci";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "ime";
    public static final String COLUMN_SURNAME = "prezime";
    public static final String COLUMN_HEIGHT = "visina";
    public static final String COLUMN_GLUCOSE = "glukoza";
    public static final String COLUMN_INSULIN = "inzulin";
    public static final String COLUMN_BP = "krvni_tlak";
    public static final String COLUMN_WEIGHT = "masa";
    public static final String COLUMN_FATS = "masti";
    public static final String COLUMN_CARBS = "ugljikohidrati";
    public static final String COLUMN_CALORIES = "kalorije";
    public static final String COLUMN_RADIOBUTTON1 = "button1";
    public static final String COLUMN_RADIOBUTTON2 = "button2";


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE1);
        db.execSQL(CREATE_TABLE2);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_1_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_2_NAME);
        onCreate(db);

    }


    private static final String CREATE_TABLE1 =
            "CREATE TABLE " + TABLE_1_NAME + " (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + COLUMN_NAME + " VARCHAR(255), " + COLUMN_SURNAME + " VARCHAR(15), "
                    + COLUMN_HEIGHT + " DOUBLE, " + COLUMN_RADIOBUTTON1  + " INTEGER, " + COLUMN_RADIOBUTTON2 + " INTEGER);";

    private static final String CREATE_TABLE2 =
            "CREATE TABLE " + TABLE_2_NAME + " (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + COLUMN_GLUCOSE + " DOUBLE, " + COLUMN_INSULIN + " DOUBLE, "
                    + COLUMN_BP + " DOUBLE, " + COLUMN_WEIGHT + " DOUBLE, " + COLUMN_FATS + " DOUBLE, "
                    + COLUMN_CARBS + " DOUBLE, " + COLUMN_CALORIES + " DOUBLE);";


    public boolean insertDataforuser(String name, String surname, String height, int radio1, int radio2) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, name);
        values.put(COLUMN_SURNAME, surname);
        values.put(COLUMN_HEIGHT, height);
        values.put(COLUMN_RADIOBUTTON1, radio1);
        values.put(COLUMN_RADIOBUTTON2, radio2);
        long result = db.insert(TABLE_1_NAME, null, values);
        if (result == -1)
            return false;
        else
            return true;
    }

    public boolean insertData(String glucose, String insulin, String bp, String mass, String fat, String carbs, String calories) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values2 = new ContentValues();
        values2.put(COLUMN_GLUCOSE, glucose);
        values2.put(COLUMN_INSULIN, insulin);
        values2.put(COLUMN_BP, bp);
        values2.put(COLUMN_WEIGHT, mass);
        values2.put(COLUMN_FATS, fat);
        values2.put(COLUMN_CARBS, carbs);
        values2.put(COLUMN_CALORIES, calories);
        long result = db.insert(TABLE_2_NAME, null, values2);
        if (result == -1)
            return false;
        else
            return true;
    }

    public double[] getAvgGlucose(){

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor r = db.rawQuery("select * from " + TABLE_2_NAME, null);
        double polje[] = new double[r.getCount()];
        for (r.moveToFirst(); !r.isAfterLast(); r.moveToNext()){
            polje[r.getPosition()] = r.getDouble(1);
        }
        return polje;
    }

    public double[] getAvgInsulin(){

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor r = db.rawQuery("select * from " + TABLE_2_NAME, null);
        double polje[] = new double[r.getCount()];
        for (r.moveToFirst(); !r.isAfterLast(); r.moveToNext()){
            polje[r.getPosition()] = r.getDouble(2);
        }
        return polje;
    }

    public double[] getAvgBp(){

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor r = db.rawQuery("select * from " + TABLE_2_NAME, null);
        double polje[] = new double[r.getCount()];
        for (r.moveToFirst(); !r.isAfterLast(); r.moveToNext()){
            polje[r.getPosition()] = r.getDouble(3);
        }
        return polje;
    }

    public double[] getAvgMass(){

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor r = db.rawQuery("select * from " + TABLE_2_NAME, null);
        double polje[] = new double[r.getCount()];
        for (r.moveToFirst(); !r.isAfterLast(); r.moveToNext()){
            polje[r.getPosition()] = r.getDouble(3);
        }
        return polje;
    }

    public double[] getAvgFat(){

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor r = db.rawQuery("select * from " + TABLE_2_NAME, null);
        double polje[] = new double[r.getCount()];
        for (r.moveToFirst(); !r.isAfterLast(); r.moveToNext()){
            polje[r.getPosition()] = r.getDouble(3);
        }
        return polje;
    }

    public double[] getAvgCarbs(){

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor r = db.rawQuery("select * from " + TABLE_2_NAME, null);
        double polje[] = new double[r.getCount()];
        for (r.moveToFirst(); !r.isAfterLast(); r.moveToNext()){
            polje[r.getPosition()] = r.getDouble(6);
        }
        return polje;
    }

    public double[] getAvgCalories(){

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor r = db.rawQuery("select * from " + TABLE_2_NAME, null);
        double polje[] = new double[r.getCount()];
        for (r.moveToFirst(); !r.isAfterLast(); r.moveToNext()){
            polje[r.getPosition()] = r.getDouble(7);
        }
        return polje;
    }


    public Cursor getAllDataForUser() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_1_NAME, null);
        return res;
    }

    public Cursor getAllData(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res1 = db.rawQuery("select * from " + TABLE_2_NAME, null);
        return res1;
    }

    public Double getLastGlucoseEntry(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_2_NAME, null);
        res.moveToLast();
        Double glucose = res.getDouble((1));
        db.close();
        return glucose;
    }

    public Double getLastInsulinEntry(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_2_NAME, null);
        res.moveToLast();
        Double insulin = res.getDouble((2));
        return insulin;
    }

    public String getUserName(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor crs= db.rawQuery("select * from " + TABLE_1_NAME, null);
        crs.moveToLast();
        String ime = crs.getString(1);
        return ime;
    }

    public String getUserLastname(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor crs= db.rawQuery("select * from " + TABLE_1_NAME, null);
        crs.moveToLast();
        String prezime = crs.getString(2);
        return prezime;
    }

    public String getUserHeight(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor crs= db.rawQuery("select * from " + TABLE_1_NAME, null);
        crs.moveToLast();
        String visina = crs.getString(3);
        return visina;
    }

    public int getRadioButton1Value(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor crs= db.rawQuery("select * from " + TABLE_1_NAME, null);
        crs.moveToLast();
        return crs.getInt(4);
    }

    public int getRadioButton2Value(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor crs= db.rawQuery("select * from " + TABLE_1_NAME, null);
        crs.moveToLast();
        return crs.getInt(5);
    }

}





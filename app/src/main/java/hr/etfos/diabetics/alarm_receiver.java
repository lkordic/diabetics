package hr.etfos.diabetics;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class alarm_receiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {


        //dohvati extra stringove iz namjere (namjere su u aktivnosti podsjetnik)
        String get_string = intent.getExtras().getString("extra");

        Log.e("koji je ključ?", get_string);

        //namjera za pozivanje usluge za reprodukciju
        Intent service_intent = new Intent(context, Ringtone.class);

        //proslijedi extra string iz podsjetnika u ringtonePlayingService
        service_intent.putExtra("extra",get_string);

        //započni uslugu preko namjere
        context.startService(service_intent);
    }
}


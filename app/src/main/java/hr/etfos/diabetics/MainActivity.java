package hr.etfos.diabetics;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    DBHelper myHelper;
    Intent intent;
    RadioGroup radiogroup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myHelper = new DBHelper(this);
        intent = getIntent();

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        final RadioButton rb1 = (RadioButton) findViewById(R.id.rb_1);
        final RadioButton rb2 = (RadioButton) findViewById(R.id.rb_2);
        final EditText edtInput = (EditText)findViewById(R.id.editText);
        final EditText edtInput2 = (EditText)findViewById(R.id.editText2);
        final EditText visina= (EditText)findViewById(R.id.editText3);
        final Button btn_info = (Button) findViewById(R.id.btn_info);
        Button button = (Button) findViewById(R.id.button_ok);
        radiogroup = (RadioGroup)findViewById(R.id.radioGroup);


        if (Objects.equals(intent.getStringExtra("test"), "edit")){
            assert edtInput != null;
            edtInput.setText(intent.getStringExtra("ime"));
            assert edtInput2 != null;
            edtInput2.setText(intent.getStringExtra("prezime"));
            assert visina != null;
            visina.setText(intent.getStringExtra("visina"));
            if(intent.getIntExtra("radio1", 0) == 1) {
                assert rb1 != null;
                rb1.setChecked(true);
            }
            if(intent.getIntExtra("radio2", 0) == 1) {
                assert rb2 != null;
                rb2.setChecked(true);
            }
        }

        //popup window
        assert btn_info != null;
        btn_info.setOnClickListener(new View.OnClickListener(){
                   public void onClick(View v){
                    startActivity(new Intent(MainActivity.this,Pop.class));
                   }
            }
        );


        assert button != null;
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if(edtInput.getText().toString().length() == 0 || edtInput2.getText().toString().length() == 0
                        || visina.getText().toString().length()==0 || (!rb1.isChecked() && !rb2.isChecked()) )
                {
                    Toast.makeText(MainActivity.this, "Upišite ime i prezime i odaberite tip dijabetesa", Toast.LENGTH_LONG).show();
                }
                else {
                    myHelper.insertDataforuser(edtInput.getText().toString(), edtInput2.getText().toString(), visina.getText().toString(), rb1.isChecked()?1:0,
                            rb2.isChecked()?1:0);
                    Toast.makeText(MainActivity.this, "Podaci spremljeni", Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(getApplicationContext(), HomeScreenActivity.class);
                    startActivity(intent);
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on THE Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.promjeni) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

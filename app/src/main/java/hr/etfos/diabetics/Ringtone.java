package hr.etfos.diabetics;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.EditText;

public class Ringtone extends Service{
    MediaPlayer media_song;
    int startId;
    boolean isRunning;
    EditText opis_alarma;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    public int onStartCommand(Intent intent, int flags, int startId){

        String state = intent.getExtras().getString("extra");

//pretvaranje extra stringova iz intenta
        assert state != null;
        switch(state){
            case "Upaljen":
                startId=1;
                break;
            case "Ugašen":
                startId=0;
                break;
            default:
                startId=0;
                break;
        }


        if(!this.isRunning && startId ==1){
            media_song = MediaPlayer.create(this, R.raw.awesomemorning_alarm);
            media_song.start();

            this.isRunning=true;
            this.startId = 0;

            NotificationManager notification_manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            Intent intent_podsjetnik = new Intent (this.getApplicationContext(), Alarm_activity.class);
            PendingIntent pending_notify = PendingIntent.getActivity(this, 0, intent_podsjetnik, 0);

            Notification notification_popup = new Notification.Builder(this).
                    setContentTitle("Izmjerite razinu šećera! Uzmite inuzlin!").setContentText("Pritisni za gašenje alarma").setContentIntent
                    (pending_notify).setAutoCancel(true).setSmallIcon(R.drawable.kap_krvi).build();

            notification_manager.notify(0, notification_popup);

        }
        //ako melodija svira i korisnik pritisne gumb za gašenje - ugasi alarm
        else if(this.isRunning && startId ==0){

            //zaustavlja melodiju
            media_song.stop();
            media_song.reset();

            this.isRunning = false;
            this.startId = 0;
        }
        //ako svira melodija i korisnik pritisne gumb za paljenje - ne radi ništa
        else if(this.isRunning && startId ==1){

            this.isRunning = true;
            this.startId = 1;
        }
        //ako melodija ne svira i korisnik pritisne gumb za gašenje - ne radi ništa
        else if (!this.isRunning && startId ==0){
            this.isRunning = false;
            this.startId = 0;
        }
        // neki posebni slučajevi (gašenje aplikacije itd.)
        else {

        }




        //neće se ponovno upaliti u slučaju neočekivanog prekida aplikacije
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy(){

        super.onDestroy();
        this.isRunning=false;
    }
}



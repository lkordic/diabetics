package hr.etfos.diabetics;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.sql.Time;
import java.util.Calendar;

public class Alarm_activity extends AppCompatActivity {
    AlarmManager alarm_manager;
    TimePicker timepicker;
    Context context;
    PendingIntent pending_intent;
    DatePicker datepicker;
    Button postavi;
    Button zaustavi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_activity);
        this.context = this;
        alarm_manager = (AlarmManager) getSystemService(ALARM_SERVICE);
        datepicker = (DatePicker) findViewById(R.id.datepicker);
        timepicker = (TimePicker) findViewById(R.id.timepicker);
        final Calendar calendar = Calendar.getInstance();
        postavi = (Button) findViewById(R.id.button_postavi);
        zaustavi = (Button) findViewById(R.id.button_zaustavi);

        final Intent my_intent = new Intent(this.context, alarm_receiver.class);

        assert postavi != null;
        postavi.setOnClickListener(new View.OnClickListener() {

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                //postavljanje instance kalendara na odabrano vrijeme
                calendar.set(Calendar.HOUR_OF_DAY, timepicker.getCurrentHour());
                calendar.set(Calendar.MINUTE, timepicker.getCurrentMinute());
                calendar.set(Calendar.DAY_OF_MONTH, datepicker.getDayOfMonth());
                calendar.set(Calendar.MONTH, datepicker.getMonth());
                calendar.set(Calendar.YEAR, datepicker.getYear());

                //dohvati int vrijednosti parametara
                int hour = timepicker.getCurrentHour();
                int minute = timepicker.getCurrentMinute();
                int date1 = datepicker.getDayOfMonth();
                int month = datepicker.getMonth();
                int year = datepicker.getYear();

                month = month + 1;

                //pretvori int vrijednosti u string za ispis u textboxu
                String hour_string = String.valueOf(hour);
                String minute_string = String.valueOf(minute);
                String date_string = String.valueOf(date1);
                String month_string = String.valueOf(month);
                String year_string = String.valueOf(year);

                //if petlja za dodavanje 0 ispred jedinične minute
                if (minute < 10) {
                    minute_string = "0" + String.valueOf(minute);
                }
                Toast.makeText(Alarm_activity.this, "Alarm postavljen na" + hour_string + ":" + minute_string + ", " + date_string +
                        "." + month_string + "." + year_string + ".", Toast.LENGTH_SHORT).show();

                //dodaj extra string (ključ = extra), reci programu da smo pritisnuli gumb "upali alarm"
                my_intent.putExtra("extra", "Upaljen");
                pending_intent = PendingIntent.getBroadcast(Alarm_activity.this, 0, my_intent, PendingIntent.FLAG_UPDATE_CURRENT);
                alarm_manager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pending_intent);
            }
        });

        //funkcija koja se pokreće pritiskom na gumb za gašenje
        assert zaustavi != null;
        zaustavi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Alarm_activity.this, "Alarm zaustavljen", Toast.LENGTH_SHORT).show();
                alarm_manager.cancel(pending_intent);
                my_intent.putExtra("extra", "Ugašen");
                //zaustavi melodiju
                sendBroadcast(my_intent);


            }
        });
    }
}

package hr.etfos.diabetics;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

/**
 * Created by Luka on 26.8.2016..
 */
public class Values_activity extends AppCompatActivity{
    DBHelper myHelper;
    EditText glukoza;
    EditText inzulin;
    EditText krvni_tlak;
    EditText masa;
    EditText ugljikohidrati;
    EditText masti;
    EditText kalorije;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_novo);
        myHelper = new DBHelper(this);

        glukoza = (EditText)findViewById(R.id.glukoza);
        inzulin = (EditText)findViewById(R.id.inzulin);
        krvni_tlak = (EditText)findViewById(R.id.krvnitlak);
        masa = (EditText)findViewById(R.id.masa);
        ugljikohidrati = (EditText)findViewById(R.id.ugljikohidrati);
        masti = (EditText)findViewById(R.id.masti);
        kalorije = (EditText)findViewById(R.id.kalorije);



    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on THE Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.save){
                    myHelper.insertData(glukoza.getText().toString(), inzulin.getText().toString(), krvni_tlak.getText().toString(),
                            masa.getText().toString(), masti.getText().toString(), ugljikohidrati.getText().toString(),
                            kalorije.getText().toString());

            Toast.makeText(Values_activity.this, "Podaci spremljeni", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, HomeScreenActivity.class);
            startActivity(intent);
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.promjeni) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
